package rkaran.football.utils

import com.fasterxml.jackson.databind.ObjectMapper

/**
 * Created by rkaran on 26/02/17.
 */
object JsonConverter {

    private val mapper = ObjectMapper()

     fun <T> fromJson(json:String, cls:Class<T>):T {
         try {
             return this.mapper.readValue(json, cls)
         } catch(e:Exception) {
             throw RuntimeException(e)
         }
    }

    fun <T> toJson(obj:T):String {
        try {
            return this.mapper.writeValueAsString(obj);
        } catch(e:Exception) {
            throw RuntimeException(e)
        }
    }

    fun <T,U> toMap(obj:T):Map<String,U> {
        try {
            return this.mapper.convertValue(obj, Map::class.java) as Map<String,U>
        } catch(e:Exception) {
            throw RuntimeException(e)
        }
    }

    fun jsonToMap(json:String):Map<String,*> {
        try {
            return this.mapper.readValue(json, Map::class.java) as Map<String,*>
        } catch(e:Exception) {
            throw RuntimeException(e);
        }
    }
}
