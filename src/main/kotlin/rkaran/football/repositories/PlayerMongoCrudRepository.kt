package rkaran.football.repositories

import rkaran.football.dbhandlers.DBHandler
import rkaran.football.dbhandlers.MongoHandler

/**
 * Created by rkaran on 09/11/17.
 */
object PlayerMongoCrudRepository:PlayerCrudRepository() {

    override fun getAllIds():List<String> = MongoHandler.getAllIds(this.getDB(), this.getCollection())

    override fun getDBHandler():DBHandler = MongoHandler
}