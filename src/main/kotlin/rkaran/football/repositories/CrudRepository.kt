package rkaran.football.repositories

import rkaran.football.pojos.Entity

/**
 * Created by rkaran on 07/11/17.
 */
open abstract class CrudRepository<T: Entity> {

    abstract fun create(obj:T):String?

    abstract fun read(id:String):T

    abstract fun update(id:String, update:T):Boolean

    abstract fun delete(id:String):Boolean
}