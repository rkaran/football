package rkaran.football.repositories

import rkaran.football.dbhandlers.DBHandler
import rkaran.football.pojos.Player
import rkaran.football.utils.ConfigReader
import rkaran.football.utils.JsonConverter

/**
 * Created by rkaran on 08/11/17.
 */
abstract class PlayerCrudRepository:CrudRepository<Player>() {

    protected fun getDB():String = ConfigReader.getString("players.db")

    protected fun getCollection():String = ConfigReader.getString("players.collection")

    override fun create(player:Player):String? {
        return this.getDBHandler().create(this.getDB(), this.getCollection(), JsonConverter.toMap(player))
    }

    override fun read(id:String):Player {
        return JsonConverter.fromJson(JsonConverter.toJson(this.getDBHandler().read(this.getDB(), this.getCollection(), id)), Player::class.java)
    }

    override fun update(id:String, update:Player):Boolean {
        return this.getDBHandler().update(this.getDB(), this.getCollection(), id, JsonConverter.toMap(update))
    }

    override fun delete(id:String):Boolean {
        return this.getDBHandler().delete(this.getDB(), this.getCollection(), id)
    }

    abstract fun getAllIds():List<String>

    abstract fun getDBHandler():DBHandler
}