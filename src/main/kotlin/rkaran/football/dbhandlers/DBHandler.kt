package rkaran.football.dbhandlers

/**
 * Created by rkaran on 09/11/17.
 */
interface DBHandler {

    fun create(db:String, collection:String, doc:Map<String,Any>):String?

    fun read(db:String, collection:String, id:String):Map<String,Any>?

    fun update(db:String, collection:String, id:String, update:Map<String,Any>):Boolean

    fun delete(db:String, collection:String, id:String):Boolean
}