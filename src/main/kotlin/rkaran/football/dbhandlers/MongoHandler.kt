package rkaran.football.dbhandlers

import com.mongodb.MongoClient
import com.mongodb.client.model.Filters.eq
import com.mongodb.client.model.Projections.fields
import org.bson.Document
import org.bson.types.ObjectId
import rkaran.football.utils.ConfigReader

/**
 * Created by rkaran on 08/11/17.
 */
object MongoHandler:DBHandler {

    private val client = with(ConfigReader.get("mongo")) {
        MongoClient(this["server"] as String, Integer.valueOf(this["port"] as String))
    }

    override fun create(db:String, collection:String, doc:Map<String,Any>):String? {
        if("_id" !in doc) {
            val mongoDoc = Document(doc)
            MongoHandler.client.getDatabase(db).getCollection(collection).insertOne(mongoDoc)
            return mongoDoc.getObjectId("_id").toString()
        } else {
            return null
        }
    }

    override fun read(db:String, collection:String, id:String):Map<String,Any>? {
        val doc = MongoHandler.client.getDatabase(db).getCollection(collection).find(eq(ObjectId(id))).singleOrNull()?.toSortedMap()
        if(null != doc) {
            doc["_id"] = doc["_id"].toString()
        }
        return doc
    }

    override fun update(db:String, collection:String, id:String, update:Map<String,Any>):Boolean =
            1 == MongoHandler.client.getDatabase(db).getCollection(collection).replaceOne(eq(ObjectId(id)), Document(update)).matchedCount.toInt()

    override fun delete(db:String, collection:String, id:String):Boolean =
            1 == MongoHandler.client.getDatabase(db).getCollection(collection).deleteOne(eq(ObjectId(id))).deletedCount.toInt()

    fun getAllIds(db:String, collection:String):List<String> {
        return MongoHandler.client.getDatabase(db).getCollection(collection).find().projection(fields()).map {
            it.getObjectId("_id").toString()
        }.toList()
    }
}