package rkaran.football.pojos

import com.fasterxml.jackson.annotation.*

/**
 * Created by rkaran on 07/11/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
class Player(id:String?, val name:String?, val age:Int?, val teamId:String?, val matches:Int?, val goals:Int?):Entity(id) {

    constructor():this(null, null, null, null, null, null)
}