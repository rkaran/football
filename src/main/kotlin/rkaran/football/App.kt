package rkaran.football

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import rkaran.football.utils.ConfigReader

@SpringBootApplication
open class App

fun main(args:Array<String>) {
    System.getProperties().putAll(ConfigReader.get("app"))
    SpringApplication.run(App::class.java, *args)
}

