package rkaran.football.controllers

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import rkaran.football.repositories.CrudRepository
import rkaran.football.pojos.Entity

/**
 * Created by rkaran on 07/11/17.
 */
open abstract class CrudController<T:Entity> {

    @RequestMapping(path = arrayOf("/", ""), method = arrayOf(RequestMethod.PUT))
    fun create(@RequestBody obj:T):ResponseEntity<String> {
        val id = this.getCrudRepository().create(obj)
        if(null != id) {
            return ResponseEntity.ok(id)
        } else {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build()
        }
    }

    @RequestMapping(path = arrayOf("/{id}/", "/{id}"), method = arrayOf(RequestMethod.GET))
    fun read(@PathVariable("id") id:String):ResponseEntity<Any> {
        val obj = this.getCrudRepository().read(id)
        if(null != obj) {
            return ResponseEntity.ok(obj)
        } else {
            return ResponseEntity.notFound().build()
        }
    }

    @RequestMapping(path = arrayOf("/{id}/", "/{id}"), method = arrayOf(RequestMethod.POST))
    fun update(@PathVariable("id") id:String, @RequestBody update:T):ResponseEntity<String> {
        if(null != update._id) {
            return ResponseEntity.badRequest().build()
        } else {
            if(this.getCrudRepository().update(id, update)) {
                return ResponseEntity.ok().build()
            } else {
                return ResponseEntity.notFound().build()
            }
        }
    }

    @RequestMapping(path = arrayOf("/{id}/", "/{id}"), method = arrayOf(RequestMethod.DELETE))
    fun delete(@PathVariable("id") id:String):ResponseEntity<String> {
        if(this.getCrudRepository().delete(id)) {
            return ResponseEntity.accepted().build()
        } else {
            return ResponseEntity.notFound().build()
        }
    }

    abstract fun getCrudRepository():CrudRepository<T>
}