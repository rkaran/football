package rkaran.football.controllers

import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController
import rkaran.football.repositories.CrudRepository
import rkaran.football.pojos.Player
import rkaran.football.repositories.PlayerCrudRepository
import rkaran.football.repositories.PlayerMongoCrudRepository

/**
 * Created by rkaran on 07/11/17.
 */
@RestController
@RequestMapping(path = arrayOf("/players"))
class PlayerCrudController:CrudController<Player>() {

    @RequestMapping(path = arrayOf("/", ""), method = arrayOf(RequestMethod.GET))
    fun getAllIds():ResponseEntity<List<String>> {
        return ResponseEntity.ok((this.getCrudRepository() as PlayerCrudRepository).getAllIds())
    }

    override fun getCrudRepository():CrudRepository<Player> = PlayerMongoCrudRepository
}