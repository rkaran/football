CRUD service for football. 

Dependencies:
Mongodb
maven3
kotlin 1.0.7

After cloning the repo, to execute the service, follow these instructions:

cd football
mvn clean install
cd target
java -jar football-1.0-SNAPSHOT.jar

Wait until you see a tail-line containing
"Started AppKt in _ seconds"

Sample curl requests for endpoints:

1. Fetch details of player, given his id: curl localhost:8080/players/{id}
2. Fetch ids of all players: curl localhost:8080/players
3. Create a new player, curl -XPUT localhost:8080/players -H'Content-Type:application/json' -d'{"name": "AJ", "age": 29, "goals": 49}'
4. Update an existing player: curl -XPOST localhost:8080/players/{id} -H'Content-Type:application/json' -d'{"name": "AJ", "age": 29, "goals": 49}'
5. Delete a player, given his id: curl -XDELETE localhost:8080/players/{id}

Note: Update replaces the existing player, so all required fields should be passed in the request body.

Player object has the following fields:
id:string
name:string
age:int
teamId:string
matches:int
goals:int